# Connect 4 AI

NOTE: This code is a work in progress and some AI may not work properly.

You can play against either another human or an AI by running the appropriate 
file (player_vs_player.py or player_vs_ai.py).

Just click to place your piece!

To select which AI you'd like to play against, type the name of one in the
terminal that shows up.
