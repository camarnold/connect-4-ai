from numpy.random import choice

# Attempts to copy the enemy's move. If it can't, picks randomly
def Copy_Cat_Policy(board):
    _, _, legal_move = board.look_ahead_move(board.last_move, board.grid.copy(), board.turn)
    if not legal_move:
        possible_moves = [i for i in range(board.grid_size)]
        possible_moves.remove(board.last_move)
    else:
        return board.last_move

    move = choice(possible_moves)
    _, _, legal_move = board.look_ahead_move(move, board.grid.copy(), board.turn)
    while not legal_move:
        possible_moves.remove(move)
        move = choice(possible_moves)
        _, _, legal_move = board.look_ahead_move(move, board.grid.copy(), board.turn)
    return move
