from numpy import unique, where
from numpy.random import choice

# attempts to move at the lowest point (ties broken randomly)
def Fill_The_Valley_Policy(board):

    # considered full by default
    height = [board.grid_size for i in range(board.grid_size)]

    for i in range(board.grid_size):
        unique_nums, num_counts = unique(board.grid[:,i], return_counts=True)
        if unique_nums[0] == 0:
            height[i] = board.grid_size - num_counts[0]

    return choice(where(height == min(height))[0])
