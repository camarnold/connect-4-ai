from numpy import array, infty, where
from numpy.random import choice

# This policy always picks the immediate best move while looking only at what
# the opponent can immediately do
def Little_Forethought_Policy(board):

    # default values to the worst possible
    your_scores = array([infty for _ in range(board.grid_size)])
    your_turn = board.turn
    if your_turn == 1:
        your_scores *= -1
        their_turn = 2
    else:
        their_turn = 1

    cur_board = board.grid.copy()
    cur_score = board.get_score(cur_board)
    for move in range(board.grid_size):
        your_scores[move] = board.update_score(move, cur_board.copy(), board.turn, prev_score=cur_score)

    # if you can win
    if abs(max(your_scores)) == 100:
        if your_turn == 1:
            return choice(where(your_scores == max(your_scores))[0])
        else:
            return choice(where(your_scores == min(your_scores))[0])

    # compute the opponent's scores
    absolute_scores = array([None for _ in range(board.grid_size)])

    their_scores = absolute_scores.copy()

    for your_move in range(board.grid_size):
        # if the game isn't over already
        if abs(your_scores[your_move]) < 100:
            new_board, _, legal = board.look_ahead_move(your_move, cur_board.copy(), your_turn)
            if legal:
                temp_scores = absolute_scores.copy()
                for their_move in range(board.grid_size):
                    temp_scores[their_move] = board.update_score(their_move, \
                        new_board.copy(), their_turn, prev_score=your_scores[your_move])
            for i in range(board.grid_size):
                if temp_scores[i] == None:
                    if their_turn == 1:
                        temp_scores[i] = -infty
                    else:
                        temp_scores[i] = infty
                # assume they make the best move they can
            if their_turn == 1:
                their_scores[your_move] = max(temp_scores)
            else:
                their_scores[your_move] = min(temp_scores)
        else:
            if their_turn == 1:
                their_scores[your_move] = -100
            else:
                their_scores[your_move] = 100

    # pick the move that limits the opponent the most
    if your_turn == 1:
        return choice(where(their_scores == max(their_scores))[0])
    else:
        return choice(where(their_scores == min(their_scores))[0])
