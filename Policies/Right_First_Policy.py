def Right_First_Policy(board):

    possible_moves = [i for i in range(board.grid_size)]
    legal_move = False
    move = possible_moves[-1]
    _, _, legal_move = board.look_ahead_move(move, board.grid.copy(), board.turn)
    while not legal_move:
        possible_moves.remove(move)
        move = choice(possible_moves)
        _, _, legal_move = board.look_ahead_move(move, board.grid.copy(), board.turn)
    return move
