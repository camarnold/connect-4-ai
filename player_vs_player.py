from classes import *
from numpy import unique
from glob import glob
from pygame import init, Rect
from pygame.event import get
from pygame.display import flip
from pygame.draw import circle, rect
from pygame.locals import MOUSEBUTTONDOWN
from pygame.mouse import get_pos
from constants import *

for i in range(GRID_SIZE):
    for j in range(GRID_SIZE):
        rect(screen, BLACK, Rect((TOTAL*i+1, TOTAL*j+1), (TOTAL-1, TOTAL-1)))

for i in range(GRID_SIZE):
    for j in range(GRID_SIZE):
        circle(screen, GRAY, (TOTAL*(i+0.5), TOTAL*(j+0.5)), TOTAL*0.45)

flip()

def click(pos):
    x = (GRID_SIZE*pos[0]) // SCREEN_SIZE # find x coord
    if 0 <= x < GRID_PIXEL_SIZE: # if the click was on the screen
        return x

if __name__ == '__main__':
    Game = Connect4()
    won = False
    while not won:
        events = get()
        for event in events:
            if event.type == MOUSEBUTTONDOWN:
                pos = get_pos()
                move = click(pos)
                if Game.turn == 1:
                    _,pos,legal = Game.look_ahead_move(move, Game.grid.copy(), 1)
                    if legal:
                        circle(screen, BLUE, (TOTAL*(pos[1]+0.5), TOTAL*(pos[0]+0.5)), TOTAL*0.45)
                        flip()
                        won = Game.move(move)

                elif Game.turn == 2:
                    _,pos,legal = Game.look_ahead_move(move, Game.grid.copy(), 2)
                    if legal:
                        circle(screen, RED, (TOTAL*(pos[1]+0.5), TOTAL*(pos[0]+0.5)), TOTAL*0.45)
                        flip()
                        won = Game.move(move)

        if unique(Game.grid)[0] != 0:
            print('It\'s a tie!')
            break
