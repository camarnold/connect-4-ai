# Will always move according to the first legal digit in Phi (doesn't work for
# grid_size >= 10)
def Phi_Lover_Policy(board):
    Phi_digits = list('1618033988749894848204586')
    Phi_digits.reverse()
    Phi_digits = [int(i) for i in Phi_digits]
    move = Phi_digits.pop()
    if 0<=move<board.grid_size:
        _, _, legal_move = board.look_ahead_move(move, board.grid.copy(), board.turn)
    while not legal_move:
        move = Phi_digits.pop()
        if 0<=move<board.grid_size:
            _, _, legal_move = board.look_ahead_move(move, board.grid.copy(), board.turn)
    return move
