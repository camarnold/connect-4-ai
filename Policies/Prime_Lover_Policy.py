from numpy.random import shuffle, choice
from math import sqrt

# attempts primes randomly first, then non primes
def Prime_Lover_Policy(board):
    def is_prime(n):
        if n % 2 == 0 and n > 2:
            return False
        return all(n % i for i in range(3, int(sqrt(n)) + 1, 2))

    primes = [2,3]
    non_primes = [0,1]
    for i in range(4, board.grid_size):
        if is_prime(i):
            primes.append(i)
        else:
            non_primes.append(i)

    shuffle(primes)
    shuffle(non_primes)

    possible_moves = primes + non_primes
    legal_move = False
    move = possible_moves[0]
    _, _, legal_move = board.look_ahead_move(move, board.grid.copy(), board.turn)
    while not legal_move:
        possible_moves.remove(move)
        move = choice(possible_moves)
        _, _, legal_move = board.look_ahead_move(move, board.grid.copy(), board.turn)
    return move
