from numpy import infty
from numpy.random import shuffle


def Minimax(board, depth):
    cur_score = board.get_score(board.grid.copy())

    # get all legal moves
    legal_moves = []
    for move in range(board.grid_size):
        _, _, legal = board.look_ahead_move(move, board.grid.copy(), board.turn)
        if legal:
            legal_moves.append(move)
    shuffle(legal_moves)

    best_move = legal_moves[0]

    if board.turn == 1:
        best_score = -infty
    else:
        best_score = infty

    alpha = -infty
    beta = infty

    for move in legal_moves:
        temp_board, _, _ = board.look_ahead_move(move, board.grid.copy(), board.turn)
        temp_new_score = board.update_score(move, board.grid.copy(), board.turn, cur_score)
        # temp_new_score = board.get_score(temp_board.copy())
        if board.turn == 1 and temp_new_score == 100:
            return move
        elif board.turn == 2 and temp_new_score == -100:
            return move

        if board.turn == 1:
            temp_board_score = Minimize_Beta(temp_board.copy(), depth-1, alpha, beta, 2, temp_new_score, board)
            # print('Move: {}, Score: {}, Best Score: {}'.format(move, temp_board_score, best_score))
            if temp_board_score > best_score:
                best_score = temp_board_score
                best_move = move
            # if best_score == 100:
            #     return best_move
        else:
            temp_board_score = Maximize_Alpha(temp_board.copy(), depth-1, alpha, beta, 1, temp_new_score, board)
            # print('Move: {}, Score: {}, Best Score: {}'.format(move, temp_board_score, best_score))
            if temp_board_score < best_score:
                best_score = temp_board_score
                best_move = move
            # if best_score == -100:
            #     return best_move

    return best_move


def Minimize_Beta(temp_board, depth, a, b, turn, score, board):
    # get all legal moves
    legal_moves = []
    for move in range(board.grid_size):
        _, _, legal = board.look_ahead_move(move, temp_board.copy(), turn)
        if legal:
            legal_moves.append(move)
    shuffle(legal_moves)

    if depth == 0 or len(legal_moves) == 0 or abs(score) == 100:
    # if we've reached our max depth or can't move or game over
        return board.get_score(temp_board.copy())#score

    beta = b
    other_turn = 1 if turn==2 else 2

    # if end of tree evaluate scores
    for move in legal_moves:
        # else continue down tree as long as ab conditions met
        if a < beta:
            new_temp_board, _, _ = board.look_ahead_move(move, temp_board.copy(), turn)
            temp_score = board.update_score(move, temp_board.copy(), turn, prev_score=score)
            # temp_score = board.get_score(new_temp_board.copy())
            temp_board_score = Maximize_Alpha(new_temp_board.copy(), depth-1, a, beta, other_turn, temp_score, board)
        beta = min(beta, temp_board_score)
    return beta


def Maximize_Alpha(temp_board, depth, a, b, turn, score, board):
    # get all legal moves
    legal_moves = []
    for move in range(board.grid_size):
        _, _, legal = board.look_ahead_move(move, temp_board.copy(), turn)
        if legal:
            legal_moves.append(move)
    shuffle(legal_moves)

    if depth == 0 or len(legal_moves) == 0 or abs(score) == 100:
    # if we've reached our max depth or can't move or game over
        return board.get_score(temp_board.copy())#score

    alpha = a
    other_turn = 1 if turn==2 else 2

    for move in legal_moves:
        if alpha < b:
            new_temp_board, _, _ = board.look_ahead_move(move, temp_board.copy(), turn)
            temp_score = board.update_score(move, temp_board.copy(), turn, prev_score=score)
            # temp_score = board.get_score(new_temp_board.copy())
            temp_board_score = Minimize_Beta(new_temp_board.copy(), depth-1, alpha, b, other_turn, temp_score, board)
        alpha = max(temp_board_score, alpha)
    return alpha
