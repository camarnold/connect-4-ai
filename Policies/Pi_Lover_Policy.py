# Will always move according to the first legal digit in pi (doesn't work for
# grid_size >= 10)
def Pi_Lover_Policy(board):
    pi_digits = list('314159265358979323846264338327950')
    pi_digits.reverse()
    pi_digits = [int(i) for i in pi_digits]
    move = pi_digits.pop()
    if 0<=move<board.grid_size:
        _, _, legal_move = board.look_ahead_move(move, board.grid.copy(), board.turn)
    while not legal_move:
        move = pi_digits.pop()
        if 0<=move<board.grid_size:
            _, _, legal_move = board.look_ahead_move(move, board.grid.copy(), board.turn)
    return move
