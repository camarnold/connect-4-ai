# Will always move according to the first legal digit in e (doesn't work for
# grid_size >= 10)
def e_Lover_Policy(board):
    e_digits = list('271828182845904523536')
    e_digits.reverse()
    e_digits = [int(i) for i in e_digits]
    move = e_digits.pop()
    if 0<=move<board.grid_size:
        _, _, legal_move = board.look_ahead_move(move, board.grid.copy(), board.turn)
    while not legal_move:
        move = e_digits.pop()
        if 0<=move<board.grid_size:
            _, _, legal_move = board.look_ahead_move(move, board.grid.copy(), board.turn)
    return move
