from Policies.Minimax_With_Alpha_Beta_Pruning import *
from numpy import infty, where, array
from numpy.random import choice

def Minimax_Policy(board):
    return Minimax(board, depth=5)
