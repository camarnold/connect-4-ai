from pygame import init
from pygame.display import set_mode

BLUE = (0,0,255)
RED = (255,0,0)
BLACK = (0,0,0)
GRAY = (150,150,150)

init()

GRID_SIZE = 7
GRID_PIXEL_SIZE = 100
SPACE_BETWEEN_PIXELS = 2
TOTAL = GRID_PIXEL_SIZE + SPACE_BETWEEN_PIXELS
SCREEN_SIZE = GRID_SIZE*TOTAL + 1

screen = set_mode((SCREEN_SIZE, SCREEN_SIZE))
screen.fill(GRAY)
