from numpy import array, infty, where
from numpy.random import choice

# This policy always picks the immediate best move and doesn't look to the future
def Selfish_Policy(board):

    # default values to the worst possible
    scores = array([infty for _ in range(board.grid_size)])
    if board.turn == 1:
        scores *= -1

    cur_board = board.grid.copy()
    cur_score = board.get_score(cur_board)
    for move in range(board.grid_size):
        scores[move] = board.update_score(move, cur_board.copy(), board.turn, prev_score=cur_score)

    # pick the best move (random if tie)
    if board.turn == 1:
        return choice(where(scores == max(scores))[0])
    else:
        return choice(where(scores == min(scores))[0])
