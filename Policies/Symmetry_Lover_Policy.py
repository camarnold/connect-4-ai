from numpy.random import choice

# Attempts to make the board symmetrical by symmetrically copying the enemy's
# moves.  If the enemy fills the middle column, moves randomly
def Symmetry_Lover_Policy(board):
    _, _, legal_move = board.look_ahead_move(board.grid_size-1-board.last_move, board.grid.copy(), board.turn)
    if not legal_move:
        possible_moves = [i for i in range(board.grid_size)]
        possible_moves.remove(board.grid_size-1-board.last_move)
    else:
        return board.grid_size-1-board.last_move

    move = choice(possible_moves)
    _, _, legal_move = board.look_ahead_move(move, board.grid.copy(), board.turn)
    while not legal_move:
        possible_moves.remove(move)
        move = choice(possible_moves)
        _, _, legal_move = board.look_ahead_move(move, board.grid.copy(), board.turn)
    return move
