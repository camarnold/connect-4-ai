from numpy import unique, where
from numpy.random import choice

# attempts to move at the highest point (ties broken randomly)
def Stack_It_Up_Policy(board):

    # considered empty by default
    height = [0 for i in range(board.grid_size)]

    for i in range(board.grid_size):
        unique_nums, num_counts = unique(board.grid[:,i], return_counts=True)
        if unique_nums[0] == 0:
            height[i] = board.grid_size - num_counts[0]

    return choice(where(height == max(height))[0])
