from numpy import array, unique, clip
from numpy.random import randint


class Connect4():

    def __init__(self, grid_size=7, win_con=4):
        self.grid_size = grid_size # 7x7 grid
        self.win_con = win_con # need 4 in a row
        self.colors = [(255,255,255), (0,0,255), (255,0,0)]
        self.grid = array([[0 for j in range(self.grid_size)] for i in range(self.grid_size)])

        self.turn = 1
        self.last_move = randint(0, self.grid_size)
        self.show()

    # can take in custom board and turn
    def move(self, pos, show=False):

        # if it's a legal number
        if pos < 0 or pos >= self.grid_size:
            print('Illegal Move: Move out of bounds')
            return
        # or if the column is filled
        elif self.grid[0, pos] != 0:
            print('Illegal Move: Column full')
            return

        _, indices = unique(self.grid[:, pos], return_counts=True)
        self.grid[indices[0]-1, pos] = self.turn

        if show:
            self.show()

        won = self.check_win()
        self.last_move = pos

        if won:
            print('Player {} wins!'.format(self.turn))
            return won
        self.turn = 2 if self.turn == 1 else 1
        return won


    def look_ahead_move(self, pos, grid, turn):

        # if the column is filled
        if self.grid[0, pos] != 0:
            return grid, None, False

        _, indices = unique(grid[:, pos], return_counts=True)
        grid[indices[0]-1, pos] = turn

        return grid, [indices[0]-1, pos], True


    # The score will be a single number for both players (positive means
    # player 1 favored, negative will be player 2 favored).
    # We will award a score based on how many runs of varying sizes a player has
    # OPEN, that is, runs that can be built on in future turns.
    # A player will get no points for any piece in isolation (a 1-run),
    # +1 for each 2-run, +5 for each 3-run, and the total score is SET to 100
    # for winning.
    def get_score(self, board):
        score = 0
        # horizontal
        for j in range(self.grid_size):
            cur_row = board[j]
            best_1 = 0
            best_2 = 0
            prev = 0
            empty_before = False
            for i in range(self.grid_size):
                if cur_row[i] == 1:
                    # gives player 2 score if worthy
                    if best_2 > 1 and empty_before:
                        if best_2 == 2:
                            score -= 1
                        elif best_2 == 3:
                            score -= 5
                        elif best_2 == 4:
                            return -100
                    best_2 = 0
                    empty_before = False
                    best_1 += 1
                    prev = 1
                    if prev == 0:
                        empty_before = True
                elif cur_row[i] == 2:
                    if best_1 > 1 and empty_before:
                        if best_1 == 2:
                            score += 1
                        elif best_1 == 3:
                            score += 5
                        elif best_1 == 4:
                            return 100
                    best_1 = 0
                    empty_before = False
                    best_2 += 1
                    prev = 2
                    if prev == 0:
                        empty_before = True
                else:
                    if best_1 > 1:
                        if best_1 == 2:
                            score += 1
                        elif best_1 == 3:
                            score += 5
                        elif best_1 == 4:
                            return 100
                    elif best_2 > 1:
                        if best_2 == 2:
                            score -= 1
                        elif best_2 == 3:
                            score -= 5
                        elif best_2 == 4:
                            return -100
                    best_1 = 0
                    best_2 = 0
                    prev = 0
                    empty_before = True
            if best_1 > 1:
                if best_1 == 2:
                    score += 1
                elif best_1 == 3:
                    score += 5
                elif best_1 == 4:
                    return 100
            elif best_2 > 1:
                if best_2 == 2:
                    score -= 1
                elif best_2 == 3:
                    score -= 5
                elif best_2 == 4:
                    return -100

        for i in range(self.grid_size):
            best_1 = 0
            best_2 = 0
            prev = 0
            empty_before = False
            for j in range(self.grid_size):
                if board[j, i] == 1:
                    # gives player 2 score if worthy
                    if best_2 > 1 and empty_before:
                        if best_2 == 2:
                            score -= 1
                        elif best_2 == 3:
                            score -= 5
                        elif best_2 == 4:
                            return -100
                    best_2 = 0
                    empty_before = False
                    best_1 += 1
                    prev = 1
                    if prev == 0:
                        empty_before = True
                elif board[j, i] == 2:
                    if best_1 > 1 and empty_before:
                        if best_1 == 2:
                            score += 1
                        elif best_1 == 3:
                            score += 5
                        elif best_1 == 4:
                            return 100
                    best_1 = 0
                    empty_before = False
                    best_2 += 1
                    prev = 2
                    if prev == 0:
                        empty_before = True
                elif board[j, i] == 0:
                    if best_1 > 1:
                        if best_1 == 2:
                            score += 1
                        elif best_1 == 3:
                            score += 5
                        elif best_1 == 4:
                            return 100
                    elif best_2 > 1:
                        if best_2 == 2:
                            score -= 1
                        elif best_2 == 3:
                            score -= 5
                        elif best_2 == 4:
                            return -100
                    best_1 = 0
                    best_2 = 0
                    prev = 0
                    empty_before = True
            if best_1 > 1:
                if best_1 == 2:
                    score += 1
                elif best_1 == 3:
                    score += 5
                elif best_1 == 4:
                    return 100
            elif best_2 > 1:
                if best_2 == 2:
                    score -= 1
                elif best_2 == 3:
                    score -= 5
                elif best_2 == 4:
                    return -100

        for i in range(self.grid_size):#-self.win_con):
            best_1 = 0
            best_2 = 0
            prev = 0
            empty_before = False
            for k in range(self.grid_size-i):
                if board[i+k, k] == 1:
                    # gives player 2 score if worthy
                    if best_2 > 1 and empty_before:
                        if best_2 == 2:
                            score -= 1
                        elif best_2 == 3:
                            score -= 5
                        elif best_2 == 4:
                            return -100
                    best_2 = 0
                    empty_before = False
                    best_1 += 1
                    prev = 1
                    if prev == 0:
                        empty_before = True
                elif board[i+k, k] == 2:
                    if best_1 > 1 and empty_before:
                        if best_1 == 2:
                            score += 1
                        elif best_1 == 3:
                            score += 5
                        elif best_1 == 4:
                            return 100
                        best_1 = 0
                        empty_before = False
                    best_2 += 1
                    prev = 2
                    if prev == 0:
                        empty_before = True
                elif board[i+k, k] == 0:
                    if best_1 > 1:
                        if best_1 == 2:
                            score += 1
                        elif best_1 == 3:
                            score += 5
                        elif best_1 == 4:
                            return 100
                    elif best_2 > 1:
                        if best_2 == 2:
                            score -= 1
                        elif best_2 == 3:
                            score -= 5
                        elif best_2 == 4:
                            return -100
                    best_1 = 0
                    best_2 = 0
                    prev = 0
                    empty_before = True
            if best_1 > 1:
                if best_1 == 2:
                    score += 1
                elif best_1 == 3:
                    score += 5
                elif best_1 == 4:
                    return 100
            elif best_2 > 1:
                if best_2 == 2:
                    score -= 1
                elif best_2 == 3:
                    score -= 5
                elif best_2 == 4:
                    return -100

        for j in range(self.grid_size):#-self.win_con):
            best_1 = 0
            best_2 = 0
            prev = 0
            empty_before = False
            for k in range(self.grid_size-i):
                if board[k, j+k] == 1:
                    # gives player 2 score if worthy
                    if best_2 > 1 and empty_before:
                        if best_2 == 2:
                            score -= 1
                        elif best_2 == 3:
                            score -= 5
                        elif best_2 == 4:
                            return -100
                    best_2 = 0
                    empty_before = False
                    best_1 += 1
                    prev = 1
                    if prev == 0:
                        empty_before = True
                elif board[k, j+k] == 2:
                    if best_1 > 1 and empty_before:
                        if best_1 == 2:
                            score += 1
                        elif best_1 == 3:
                            score += 5
                        elif best_1 == 4:
                            return 100
                        best_1 = 0
                        empty_before = False
                    best_2 += 1
                    prev = 2
                    if prev == 0:
                        empty_before = True
                elif board[k, j+k] == 0:
                    if best_1 > 1:
                        if best_1 == 2:
                            score += 1
                        elif best_1 == 3:
                            score += 5
                        elif best_1 == 4:
                            return 100
                    elif best_2 > 1:
                        if best_2 == 2:
                            score -= 1
                        elif best_2 == 3:
                            score -= 5
                        elif best_2 == 4:
                            return -100
                    best_1 = 0
                    best_2 = 0
                    prev = 0
                    empty_before = True
            if best_1 > 1:
                if best_1 == 2:
                    score += 1
                elif best_1 == 3:
                    score += 5
                elif best_1 == 4:
                    return 100
            elif best_2 > 1:
                if best_2 == 2:
                    score -= 1
                elif best_2 == 3:
                    score -= 5
                elif best_2 == 4:
                    return -100

        for i in range(self.grid_size):#-self.win_con):
            best_1 = 0
            best_2 = 0
            prev = 0
            empty_before = False
            for k in range(self.grid_size):#######################################################
                if board[(self.grid_size-1-i)-k, k] == 1:
                    # gives player 2 score if worthy
                    if best_2 > 1 and empty_before:
                        if best_2 == 2:
                            score -= 1
                        elif best_2 == 3:
                            score -= 5
                        elif best_2 == 4:
                            return -100
                    best_2 = 0
                    empty_before = False
                    best_1 += 1
                    prev = 1
                    if prev == 0:
                        empty_before = True
                elif board[(self.grid_size-1-i)-k, k] == 2:
                    if best_1 > 1 and empty_before:
                        if best_1 == 2:
                            score += 1
                        elif best_1 == 3:
                            score += 5
                        elif best_1 == 4:
                            return 100
                        best_1 = 0
                        empty_before = False
                    best_2 += 1
                    prev = 2
                    if prev == 0:
                        empty_before = True
                elif board[(self.grid_size-1-i)-k, k] == 0:
                    if best_1 > 1:
                        if best_1 == 2:
                            score += 1
                        elif best_1 == 3:
                            score += 5
                        elif best_1 == 4:
                            return 100
                    elif best_2 > 1:
                        if best_2 == 2:
                            score -= 1
                        elif best_2 == 3:
                            score -= 5
                        elif best_2 == 4:
                            return -100
                    best_1 = 0
                    best_2 = 0
                    prev = 0
                    empty_before = True
            if best_1 > 1:
                if best_1 == 2:
                    score += 1
                elif best_1 == 3:
                    score += 5
                elif best_1 == 4:
                    return 100
            elif best_2 > 1:
                if best_2 == 2:
                    score -= 1
                elif best_2 == 3:
                    score -= 5
                elif best_2 == 4:
                    return -100

        for j in range(self.grid_size):#-self.win_con):
            best_1 = 0
            best_2 = 0
            prev = 0
            empty_before = False
            for k in range(self.grid_size):################################################
                if board[k, (self.grid_size-1-j)-k] == 1:
                    # gives player 2 score if worthy
                    if best_2 > 1 and empty_before:
                        if best_2 == 2:
                            score -= 1
                        elif best_2 == 3:
                            score -= 5
                        elif best_2 == 4:
                            return -100
                    best_2 = 0
                    empty_before = False
                    best_1 += 1
                    prev = 1
                    if prev == 0:
                        empty_before = True
                elif board[k, (self.grid_size-1-j)-k] == 2:
                    if best_1 > 1 and empty_before:
                        if best_1 == 2:
                            score += 1
                        elif best_1 == 3:
                            score += 5
                        elif best_1 == 4:
                            return 100
                        best_1 = 0
                        empty_before = False
                    best_2 += 1
                    prev = 2
                    if prev == 0:
                        empty_before = True
                elif board[k, (self.grid_size-1-j)-k] == 0:
                    if best_1 > 1:
                        if best_1 == 2:
                            score += 1
                        elif best_1 == 3:
                            score += 5
                        elif best_1 == 4:
                            return 100
                    elif best_2 > 1:
                        if best_2 == 2:
                            score -= 1
                        elif best_2 == 3:
                            score -= 5
                        elif best_2 == 4:
                            return -100
                    best_1 = 0
                    best_2 = 0
                    prev = 0
                    empty_before = True
            if best_1 > 1:
                if best_1 == 2:
                    score += 1
                elif best_1 == 3:
                    score += 5
                elif best_1 == 4:
                    return 100
            elif best_2 > 1:
                if best_2 == 2:
                    score -= 1
                elif best_2 == 3:
                    score -= 5
                elif best_2 == 4:
                    return -100

        return score


    def update_score(self, move, board, turn, prev_score=None, check=False):
        # print('hello I\'ve been seen')
        # if move >= 2:
        #     breakpoint()

        if prev_score == None:
            return self.get_score(board.copy())
        else:
            score = prev_score
        new_board, new_pos, legal = self.look_ahead_move(move, board, turn)
        multi = 1 if turn==1 else -1 # to add/subtract score properly

        # 'lose' for making an illegal move (101 should prevent picking an illegal move)
        if not legal:
            return -multi*101

        y,x = new_pos

        # horizontal
        best = 0
        prev = -1
        empty_before = False
        new_piece_in_run = False # only change score if the new piece is in the run
        cur_row = new_board[y]
        # only check positions within self.win_con of new_pos AND in the grid
        for i in range(max(-self.win_con+1, -x), min(self.win_con, self.grid_size-x)):
            if cur_row[x+i] == turn:
                if i == 0:
                    new_piece_in_run = True
                best += 1
                if best == self.win_con:
                    return multi*100
            else:
                if new_piece_in_run:
                    if best > 1 and (empty_before or cur_row[x+i]==0):
                        if best == 2:
                            score += multi*1
                        elif best == 3:
                            score += multi*4 # must've been 2 before
                        # can't be 4 (would've returned immediately)
                    best = 0
                    break # already saw the new piece
                best = 0
                prev = cur_row[x+i]
                if prev == 0:
                    empty_before = True
        if best > 1 and empty_before:
            if best == 2:
                score += multi*1
            elif best == 3:
                score += multi*4
        if check:
            print('horizontal: {}'.format(score))

        # vertical
        best = 0
        prev = -1
        empty_before = False
        new_piece_in_run = False # only change score if the new piece is in the run
        cur_col = new_board[:, x]
        for i in range(max(-self.win_con+1, -y), min(self.win_con, self.grid_size-y)):
            if cur_col[y+i] == turn:
                if i == 0:
                    new_piece_in_run = True
                best += 1
                if best == self.win_con:
                    return multi*100
            else:
                if new_piece_in_run:
                    if best > 1 and (empty_before or cur_col[y+i]==0):
                        if best == 2:
                            score += multi*1
                        elif best == 3:
                            score += multi*4
                    best = 0
                    break
                best = 0
                prev = cur_row[x+i]
                if prev == 0:
                    empty_before = True
        if best > 1 and empty_before:
            if best == 2:
                score += multi*1
            elif best == 3:
                score += multi*4

        if check:
            print('vertical: {}'.format(score))

        # diagonal down_right
        best = 0
        prev = -1
        empty_before = False
        new_piece_in_run = False # only change score if the new piece is in the run
        for i in range(-self.win_con+1, self.win_con):
            try:
                if new_board[y+i, x+i] == turn:
                    if i == 0:
                        new_piece_in_run = True
                    best += 1
                    if best == self.win_con:
                        return multi*100
                else:
                    if new_piece_in_run:
                        if best > 1 and (empty_before or new_board[y+i, x+i]==0):
                            if best == 2:
                                score += multi*1
                            elif best == 3:
                                score += multi*4
                        best = 0
                        break
                    best = 0
                    prev = cur_row[x+i]
                    if prev == 0:
                        empty_before = True
            except:
                if new_piece_in_run:
                    break
                else:
                    pass
        if best > 1 and empty_before:
            if best == 2:
                score += multi*1
            elif best == 3:
                score += multi*4

        if check:
            print('diag_1: {}'.format(score))

        # diagonal up_right
        best = 0
        prev = -1
        empty_before = False
        new_piece_in_run = False # only change score if the new piece is in the run
        for i in range(-self.win_con+1, self.win_con):
            try:
                if new_board[y+i, x-i] == turn:
                    if i == 0:
                        new_piece_in_run = True
                    best += 1
                    if best == self.win_con:
                        return multi*100
                else:
                    if new_piece_in_run:
                        if best > 1 and (empty_before or new_board[y+i, x-i]==0):
                            if best == 2:
                                score += multi*1
                            elif best == 3:
                                score += multi*4
                        best = 0
                        break
                    best = 0
                    prev = cur_row[x+i]
                    if prev == 0:
                        empty_before = True
            except:
                if new_piece_in_run:
                    break
                else:
                    pass
        if best > 1 and empty_before:
            if best == 2:
                score += multi*1
            elif best == 3:
                score += multi*4

        if check:
            print('diag_2: {}'.format(score))

        return score


    def show(self):
        print(self.grid)


    def check_win(self):
        won = self._check_horizontal(self.turn)
        if won:
            return won
        won = self._check_vertical(self.turn)
        if won:
            return won
        won = self._check_diagonal(self.turn)
        return won


    def _check_horizontal(self, turn):
        for j in range(self.grid_size):
            best = 0
            cur_row = self.grid[j]
            for i in range(self.grid_size):
                if cur_row[i] == turn:
                    best += 1
                    if best == self.win_con:
                        return True
                else:
                    best = 0
        return False


    def _check_vertical(self, turn):
        for i in range(self.grid_size):
            best = 0
            for j in range(self.grid_size):
                if self.grid[j, i] == turn:
                    best += 1
                    if best == self.win_con:
                        return True
                else:
                    best = 0
        return False


    def _check_diagonal(self, turn):
        for i in range(self.grid_size-self.win_con):
            best = 0
            for k in range(self.grid_size-i):
                if self.grid[i+k, k] == turn:
                    best += 1
                    if best == self.win_con:
                        return True
                else:
                    best = 0

        for j in range(self.grid_size-self.win_con):
            best = 0
            for k in range(self.grid_size-i):
                if self.grid[k, j+k] == turn:
                    best += 1
                    if best == self.win_con:
                        return True
                else:
                    best = 0


        for i in range(self.grid_size-self.win_con):
            best = 0
            for k in range(self.grid_size-i):
                if self.grid[(self.grid_size-1-i)-k, k] == turn:
                    best += 1
                    if best == self.win_con:
                        return True
                else:
                    best = 0

        for j in range(self.grid_size-self.win_con):
            best = 0
            for k in range(self.grid_size-j):
                if self.grid[k, (self.grid_size-1-j)-k] == turn:
                    best += 1
                    if best == self.win_con:
                        return True
                else:
                    best = 0

        return False
