from classes import *
from numpy import unique
from Policies import *
from glob import glob
from pygame import init, Rect
from pygame.event import get
from pygame.display import flip
from pygame.draw import circle, rect
from pygame.locals import MOUSEBUTTONDOWN
from pygame.mouse import get_pos
from constants import *

for i in range(GRID_SIZE):
    for j in range(GRID_SIZE):
        rect(screen, BLACK, Rect((TOTAL*i+1, TOTAL*j+1), (TOTAL-1, TOTAL-1)))

for i in range(GRID_SIZE):
    for j in range(GRID_SIZE):
        circle(screen, GRAY, (TOTAL*(i+0.5), TOTAL*(j+0.5)), TOTAL*0.45)

flip()

def click(pos):
    x = (GRID_SIZE*pos[0]) // SCREEN_SIZE # find x coord
    if 0 <= x < GRID_PIXEL_SIZE: # if the click was on the screen
        return x



if __name__ == '__main__':

    # choose policy to play against:
    print('Choose of the following policies to play against:\n')
    policy_choices = glob('Policies\\*_Policy.py')
    options = []
    for file in policy_choices:
        options.append(file[9:-10])
        print(file[9:-10])
    print()
    chosen_policy = ''
    while chosen_policy not in options:
        chosen_policy = input()
        if chosen_policy not in options:
            print('Check your spelling!\n')

    if chosen_policy == 'Selfish':
        policy = Selfish_Policy.Selfish_Policy
    elif chosen_policy == 'Little_Forethought':
        policy = Little_Forethought_Policy.Little_Forethought_Policy
    elif chosen_policy == 'Random':
        policy = Random_Policy.Random_Policy
    elif chosen_policy == 'Left_First':
        policy = Left_First_Policy.Left_First_Policy
    elif chosen_policy == 'Right_First':
        policy = Right_First_Policy.Right_First_Policy
    elif chosen_policy == 'Stack_It_Up':
        policy = Stack_It_Up_Policy.Stack_It_Up_Policy
    elif chosen_policy == 'Fill_The_Valley':
        policy = Fill_The_Valley_Policy.Fill_The_Valley_Policy
    elif chosen_policy == 'Copy_Cat':
        policy = Copy_Cat_Policy.Copy_Cat_Policy
    elif chosen_policy == 'Symmetry_Lover':
        policy = Symmetry_Lover_Policy.Symmetry_Lover_Policy
    elif chosen_policy == 'Pi_Lover':
        policy = Pi_Lover_Policy.Pi_Lover_Policy
    elif chosen_policy == 'e_Lover':
        policy = e_Lover_Policy.e_Lover_Policy
    elif chosen_policy == 'Phi_Lover':
        policy = Phi_Lover_Policy.Phi_Lover_Policy
    elif chosen_policy == 'Little_Phi_Lover':
        policy = Little_Phi_Lover_Policy.Little_Phi_Lover_Policy
    elif chosen_policy == 'Prime_Lover':
        policy = Prime_Lover_Policy.Prime_Lover_Policy
    elif chosen_policy == 'Prime_Hater':
        policy = Prime_Hater_Policy.Prime_Hater_Policy
    elif chosen_policy == 'Minimax':
        policy = Minimax_Policy.Minimax_Policy


    Game = Connect4()
    won = False
    while not won:
        events = get()
        for event in events:
            if event.type == MOUSEBUTTONDOWN:
                pos = get_pos()
                my_input = click(pos)
                if Game.turn == 1:
                    _,pos,_ = Game.look_ahead_move(my_input, Game.grid.copy(), 1)
                    won = Game.move(my_input)
                    circle(screen, BLUE, (TOTAL*(pos[1]+0.5), TOTAL*(pos[0]+0.5)), TOTAL*0.45)
                    flip()

        if Game.turn == 2:
            move = policy(Game)
            _,pos,_ = Game.look_ahead_move(move, Game.grid.copy(), 2)
            won = Game.move(move)
            circle(screen, RED, (TOTAL*(pos[1]+0.5), TOTAL*(pos[0]+0.5)), TOTAL*0.45)
            flip()

        # if there are no more empty spaces and no one has won
        if unique(Game.grid,)[0] != 0:
            print('It\'s a tie!')
            break
